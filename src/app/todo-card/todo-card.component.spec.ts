import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoCardComponent } from './todo-card.component';
import { CardModule } from 'primeng/card';
import { CommonModule } from '@angular/common';
import { ITask } from '../model/task';

describe('TodoCardComponent', () => {
  let component: TodoCardComponent;
  let fixture: ComponentFixture<TodoCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CardModule,CommonModule,TodoCardComponent],


    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TodoCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should emit onDelete event with task id', () => {
    const taskId = 1;
    spyOn(component.onDelete, 'emit');
    component.task = { id: taskId, title: 'Task Title', content: 'Task Content', completed: false, deadline: new Date() } as ITask;
    component.delete();
    expect(component.onDelete.emit).toHaveBeenCalledWith(taskId);
  });

  it('should emit onEdit event with task object', () => {
    const mockTask: ITask = { id: 1, title: 'Task Title', content: 'Task Content', completed: false, deadline: new Date() };
    spyOn(component.onEdit, 'emit');
    component.task = mockTask;
    component.edit();
    expect(component.onEdit.emit).toHaveBeenCalledWith(mockTask);
  });

});
