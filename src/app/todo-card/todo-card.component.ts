import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ITask } from '../model/task';
import { CardModule } from 'primeng/card';
import { CommonModule } from '@angular/common';
@Component({
  selector: 'app-todo-card',
  standalone: true,
  imports: [CardModule,CommonModule],
  templateUrl: './todo-card.component.html',
  styleUrl: './todo-card.component.scss'
})
export class TodoCardComponent {
  @Input() task!: ITask;
  @Output() onDelete: EventEmitter<number> = new EventEmitter();
  @Output() onEdit: EventEmitter<ITask> = new EventEmitter();

  delete() {
    this.onDelete.emit(this.task.id)
  }
  edit() {
    this.onEdit.emit(this.task);
  }
}
