import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoListComponent } from './todo-list.component';
import { provideHttpClient } from '@angular/common/http';
import { ConfirmationService } from 'primeng/api';
import { TodoService } from '../services/todo.service';
import { of } from 'rxjs';
import { ITask } from '../model/task';

describe('TodoListComponent', () => {
  let component: TodoListComponent;
  let fixture: ComponentFixture<TodoListComponent>;
  let mockTodoService: jasmine.SpyObj<TodoService>;
  let mockConfirmationService: jasmine.SpyObj<ConfirmationService>;

  beforeEach(async () => {
    mockTodoService = jasmine.createSpyObj('TodoService', ['getAll', 'deleteTask', 'addTask', 'editTask']);
    mockConfirmationService = jasmine.createSpyObj('ConfirmationService', ['confirm']);
    await TestBed.configureTestingModule({
      imports: [TodoListComponent],
      providers:[provideHttpClient(),ConfirmationService]

    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TodoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call addTodo() method and set visible to true', () => {
    component.addTodo();
    expect(component.visible).toBe(true);
  });

  it('should call editTask() method and set visibleEditForm to true', () => {
    const mockTask: ITask = { id: 1, title: 'Task Title', content: 'Task Content', completed: false, deadline: new Date() };
    component.editTask(mockTask);
    expect(component.visibleEditForm).toBe(true);
  });

  it('should call add() method and add a task', () => {
    const mockTaskFormValue = {
      deadline: new Date(),
      completed: false,
      title: 'New Task',
      content: 'New Task Content'
    };
    component.taskForm.setValue(mockTaskFormValue);
    mockTodoService.addTask.and.returnValue(of(null));
    component.add();
    mockTodoService.addTask(mockTaskFormValue)
    expect(mockTodoService.addTask).toHaveBeenCalledWith(mockTaskFormValue);
  });

  it('should call edit() method and edit a task', () => {
    const mockTaskFormValue = {
      id: 1,
      deadline: new Date(),
      completed: false,
      title: 'Edited Task',
      content: 'Edited Task Content'
    };
    component.taskEditForm.setValue(mockTaskFormValue);
    mockTodoService.editTask.and.returnValue(of(null));
    component.edit();
    mockTodoService.editTask(mockTaskFormValue)
    expect(mockTodoService.editTask).toHaveBeenCalledWith(mockTaskFormValue);
  });


});
