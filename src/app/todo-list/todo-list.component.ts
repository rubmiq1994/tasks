import { Component, inject, OnDestroy, OnInit } from '@angular/core';
import { TodoCardComponent } from '../todo-card/todo-card.component';
import { TodoService } from '../services/todo.service';
import { BehaviorSubject, map, Observable, of, Subject, switchMap, takeUntil, tap } from 'rxjs';
import { CommonModule } from '@angular/common';
import { ITask } from '../model/task';
import { ButtonModule } from 'primeng/button';

import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext'
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { CheckboxModule } from 'primeng/checkbox';
import { CalendarModule } from 'primeng/calendar';
import moment from 'moment';
@Component({
  selector: 'app-todo-list',
  standalone: true,
  imports: [TodoCardComponent, CommonModule, CalendarModule, ButtonModule, ConfirmDialogModule, DialogModule, InputTextModule, ReactiveFormsModule, CheckboxModule,],
  templateUrl: './todo-list.component.html',
  styleUrl: './todo-list.component.scss'
})
export class TodoListComponent implements OnInit, OnDestroy {
  visible = false;
  visibleEditForm = false;
  taskForm!: FormGroup
  taskEditForm!: FormGroup;
  selectedTask!: ITask
  private readonly todoService = inject(TodoService);
  private readonly fb = inject(FormBuilder);
  private readonly confirmationService = inject(ConfirmationService);
  public tasks: Observable<ITask[]> = this.todoService.getAll()

  private readonly needUpdate$: BehaviorSubject<boolean> = new BehaviorSubject(
    true
  );
  private readonly destroy$: Subject<void> = new Subject();
  ngOnInit(): void {
    this.taskForm = this.fb.group({
      deadline: [null, Validators.required],
      completed: false,
      title: [null, Validators.required],
      content: [null, Validators.required]
    })
    this.taskEditForm = this.fb.group({
      deadline: [null, Validators.required],
      id: null,
      completed: false,
      title: [null, Validators.required],
      content: [null, Validators.required]
    })
    this.tasks = this.needUpdate$.pipe(
      switchMap(() => this.todoService.getAll().pipe(
        map(tasks => {
          return tasks.sort(((a:any,b:any)=>{
          return   (+a.completed) - (+b.completed)
          }))
        }),
        map(tasks => tasks.map((x: any) => {
          return {
            ...x,
            deadline: new Date(x.deadline)
          }
        }))
      ))
    )
  }
  addTodo() {
    this.visible = true
  }
  deleteTask(id: number) {
    this.confirmationService.confirm({
      message: 'Are you sure?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.todoService.deleteTask(id).pipe(takeUntil(this.destroy$)).subscribe(() => {
          this.needUpdate$.next(true)
        })

      },
      reject: () => {
      }
    })

  }
  editTask(task: ITask) {
    this.visibleEditForm = true;
    this.taskEditForm.patchValue(task);
    this.taskEditForm.get('completed')?.patchValue(task.completed ? true : false)
  }
  add() {
    if (this.taskForm.valid) {
      const deadline = this.taskForm.get('deadline')?.value;

      this.taskForm.get('deadline')?.patchValue(moment(deadline).format('YYYY-MM-DD hh:mm'))
      this.todoService.addTask(this.taskForm.value).pipe(
        takeUntil(this.destroy$),).subscribe(() => {
          this.needUpdate$.next(true);
          this.taskForm.reset();

        }, () => {
          this.taskForm.reset();
        })
    }

  }
  edit() {
    if (this.taskEditForm.valid) {
      const deadline = this.taskEditForm.get('deadline')?.value;

      this.taskEditForm.get('deadline')?.patchValue(moment(deadline).format('YYYY-MM-DD hh:mm'))
      this.todoService.editTask(this.taskEditForm.value).pipe(takeUntil(this.destroy$),).subscribe(() => {
        this.needUpdate$.next(true);
        this.taskEditForm.reset();

      },
        () => {
          this.taskEditForm.reset();
        })
    }


  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
