import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.development';
import { ITask } from '../model/task';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private readonly http: HttpClient = inject(HttpClient)

  constructor() { }
  getAll(): Observable<any> {
    return this.http.get(`${environment.API_URL}todos`, {
      headers: {
        "ngrok-skip-browser-warning": "1"
      }
    })
  }
  addTask(task: Partial<ITask>): Observable<any> {
    return this.http.post(`${environment.API_URL}todos/`, task, {
      headers: {
        "ngrok-skip-browser-warning": "1"
      }
    })

  }
  deleteTask(id: number): Observable<any> {
    return this.http.delete(`${environment.API_URL}todos/${id}`, {
      headers: {
        "ngrok-skip-browser-warning": "1"
      }
    })
  }
  editTask(task: ITask): Observable<any> {
    return this.http.put(`${environment.API_URL}todos/${task.id}`, task, {
      headers: {
        "ngrok-skip-browser-warning": "1"
      }
    })

  }
}
