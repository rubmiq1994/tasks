export interface ITask {
    readonly id: number,
    title: string,
    completed: boolean,
    content: string,
    deadline:Date
}